library platform_tab_bar_control;
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class PlatformTabBarControl extends StatefulWidget {

  /// The TabController used to control the TabView
  final TabController controller;

  /// The titles to be used on the tabs
  final List<String> tabTitles;

  /// ANDROID-ONLY: Whether the TabBar is scrollable or not. Defaults to false.
  final bool isScrollable;

  /// Set different titles for iOS Tab Bar
  ///   (might be useful since CupertinoSegmentedControl does not scroll)
  final List<String> cupertinoTabTitles;

  /// Manually set the TabBar colors
  final PlatformTabBarColors tabBarColors;

  /// Manually set the CupertinoSegmentedControl colors
  final PlatformTabSegmentedControlColors segmentedControlColors;

  PlatformTabBarControl({
    @required this.controller,
    @required this.tabTitles,
    this.isScrollable = false,
    this.cupertinoTabTitles,
    PlatformTabBarColors tabBarColors,
    PlatformTabSegmentedControlColors segmentedControlColors
  }) : assert(controller != null && tabTitles != null),
        assert(controller.length == tabTitles.length),
        //If cupertinoTabTitle IS NOT null it MUST have the same length as the material tabTitles
        assert(cupertinoTabTitles == null || cupertinoTabTitles.length == tabTitles.length),
        //We always have a class for the colors, if it's not provided null we init it without values,
        //    so the colors values can be null
        this.tabBarColors = tabBarColors ?? PlatformTabBarColors(),
        this.segmentedControlColors = segmentedControlColors ?? PlatformTabSegmentedControlColors();

  @override
  _PlatformTabBarControlState createState() => _PlatformTabBarControlState();
}

class _PlatformTabBarControlState extends State<PlatformTabBarControl> {

  int _segmentedControlValue = 0;

  @override
  void initState() {
    //The iOS Segment Control will start at the tab controller initial index
    _segmentedControlValue = widget.controller.index;

    if (Platform.isIOS) {
      //We only need this listener on iOS since the segmented control does not change automatically when the tab changes
      widget.controller.addListener(_tabViewChanged);
    }

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (Platform.isIOS) {
      return _buildCupertinoSegmentedControl();
    } else {
      return _buildMaterialTabBar();
    }
  }

  Widget _buildMaterialTabBar() {
    return TabBar(
      controller: widget.controller,
      isScrollable: widget.isScrollable,
      indicatorSize: widget.isScrollable ? TabBarIndicatorSize.label : TabBarIndicatorSize.tab,
      tabs: widget.tabTitles
          .asMap()
          .map((index, value) {
            return MapEntry(
                index,
                Tab(
                  text: value.toUpperCase(),
                )
            );
          })
          .values.toList(),
      labelColor: widget.tabBarColors.labelColor,
      indicator: widget.tabBarColors.indicatorColor == null ? null : UnderlineTabIndicator(
        borderSide: BorderSide(width: 2.0, color: widget.tabBarColors.indicatorColor),
        insets: EdgeInsets.zero
      ),
    );
  }

  Widget _buildCupertinoSegmentedControl() {
    var tabTitlesToUse = widget.cupertinoTabTitles ?? widget.tabTitles;

    return SizedBox(
      width: double.infinity,
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 8.0),
        child: CupertinoSegmentedControl(
          children: tabTitlesToUse.asMap().map((index, value) {
            return MapEntry(
                index,
                Text(value)
            );
          }),
          onValueChanged: (int newValue) {
            widget.controller.animateTo(newValue);
          },
          groupValue: _segmentedControlValue,
          borderColor: widget.segmentedControlColors.borderColor,
          selectedColor: widget.segmentedControlColors.selectedColor,
          unselectedColor: widget.segmentedControlColors.unselectedColor,
          pressedColor: widget.segmentedControlColors.pressedColor,
        ),
      ),
    );
  }

  _tabViewChanged() {
    setState(() {
      _segmentedControlValue = widget.controller.index;
    });
  }
}

class PlatformTabBarColors {
  final Color indicatorColor;
  final Color labelColor;

  PlatformTabBarColors({
    this.indicatorColor,
    this.labelColor
  });
}

class PlatformTabSegmentedControlColors {
  final Color borderColor;
  final Color selectedColor;
  final Color unselectedColor;
  final Color pressedColor;

  PlatformTabSegmentedControlColors({
    this.borderColor,
    this.selectedColor,
    this.unselectedColor,
    this.pressedColor
  });
}