# platform_tab_bar_control

A simple Tab Bar Control that uses the platforms default widgets to control a `TabBarView`.

On Android, uses the regular `TabBar`. On iOS, uses a `CupertinoSegmentedControl`.

## Usage

```dart
PlatformTabBarControl(
	controller: _tabController,
	tabTitles: ['Tab One', 'Tab Two'],
)
```

#### Further Customization

`CupertinoSegmentedControl` take the colors from the `CupertinoTheme`. If you don't have a `CupertinoTheme` configured properly you will need to specify the colors manually using the `segmentedControlColors` property:

```dart
PlatformTabBarControl(
	controller: _tabController,
	tabTitles: ['Tab One', 'Tab Two'],
	segmentedControlColors: PlatformTabSegmentedControlColors(
		borderColor: Colors.white,
		selectedColor: Colors.white,
		unselectedColor: Colors.black
   ),
)
```

Since the `CupertinoSegmentedControl` does not have an scroll sometimes you need to use smaller texts on iOS to make them fit on the screen. You can specify different texts to iOS using the `cupertinoTabTitles` property:

```dart
PlatformTabBarControl(
	controller: _tabController,
	tabTitles: ['Tab One Long Title', 'Tab Two Long Title'],
	isScrollable: true,
	cupertinoTabTitles: ['One', 'Two'],
)
```