## [0.0.4] - Added an option to control the colors of the Android TabBar widget
## [0.0.3] - Added a property to control whether the material tab bar is scrollable or not.
## [0.0.2] - Added the possibility to use different texts on Android and iOS.
## [0.0.1] - First version of the plugin.

* TODO: Describe initial release.
